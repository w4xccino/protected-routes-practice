import "./App.css";
import Views from "./Views";
import { ProtectedRoutes } from "./ProtectedRoutes";

function App() {
  return (
    <div className="App">
      <h1>Protected React Routes</h1>
      <Views />
      <ol>
        <li>
          <a href="/">Sign In Page</a>
        </li>
        <li>
          <a href="/home">Home Page</a>
        </li>
        <li>
          <a href="/account">Account Page</a>
        </li>
      </ol>
      <button
        onClick={() => {
          localStorage.setItem("rolando", true);
        }}
      >
        Log In
      </button>
      <button
        onClick={() => {
          localStorage.setItem("rolando", false);
        }}
      >
        Log Out
      </button>
    </div>
  );
}

export default App;
