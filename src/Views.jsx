import { Route, Routes } from "react-router-dom";
import Account from "./components/Account";
import SignIn from "./components/SingIn";
import Home from "./components/Home";
import { ProtectedRoutes } from "./ProtectedRoutes";

function Views() {
  return (
    <Routes>
      <Route exact path="/" element={<SignIn />} />
      <Route element={<ProtectedRoutes />}>
        <Route exact path="/home" element={<Home />} />
        <Route exact path="/account" element={<Account />} />
      </Route>
    </Routes>
  );
}
export default Views;
