import React, { useState } from "react";
import { Outlet, Navigate } from "react-router-dom";
// import SignIn from "./components/SingIn";

const ProtectedRoutes = () => {
  // const validate = (x) => {
  //   if (x == "true") {
  //     return <Outlet />;
  //   } else {
  //     return <Navigate to="/" />;
  //   }
  // };
  return localStorage.getItem("rolando") === "true" ? (
    <Outlet />
  ) : (
    <Navigate to="/" />
  );
  // return (
  //   <div>
  //     <button onClick={() => setIsAuth(true)}></button>
  //   </div>
  // );
};

export { ProtectedRoutes };
